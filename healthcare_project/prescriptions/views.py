from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from healthcare_project.prescriptions import serializers
from healthcare_project.prescriptions.models import Booking
from healthcare_project.prescriptions.permissions import BookingActionsPermission


class BookingViewSet(viewsets.ModelViewSet):
    """
    BookingViewSet
    Defines views for creating/modifying/retrieving bookings data
    """
    permission_classes = [IsAuthenticated, BookingActionsPermission, ]
    queryset = Booking.objects.all()

    def get_serializer_class(self):
        if self.request.method in ['POST', 'PATCH', 'PUT']:
            return serializers.BookingCreateEditSerializer
        return serializers.BookingListSerializer

