from rest_framework.permissions import BasePermission, SAFE_METHODS


class BookingActionsPermission(BasePermission):

    def has_permission(self, request, view):
        """
        For future I would like to check if user has appropriate prescription
        given from doctor, that'll allow him to book/buy drugs
        """
        return True
