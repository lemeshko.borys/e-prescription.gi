from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.validators import ValidationError

from healthcare_project.prescriptions.models import (
    Drug, DrugCategory, Booking, Pharmacy
)


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'last_name',
            'username',
        )


class DrugCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = DrugCategory
        fields = (
            'id',
            'name',
            'description',
        )


class DrugsSerializer(serializers.ModelSerializer):

    categories = DrugCategorySerializer(many=True, read_only=True)

    class Meta:
        model = Drug
        fields = (
            'id',
            'name',
            'description',
            'categories',
        )


class PharmacySerializer(serializers.ModelSerializer):

    class Meta:
        model = Pharmacy
        fields = (
            'id',
            'name',
            'address',
            'city',
            'state',
            'zipcode',
        )


class BookingListSerializer(serializers.ModelSerializer):

    drugs = DrugsSerializer(many=True, read_only=True)
    pharmacy = PharmacySerializer(read_only=True)
    patient = UserSerializer(read_only=True)

    class Meta:
        model = Booking
        fields = (
            'id',
            'patient',
            'pharmacy',
            'drugs',
            'created',
            'modified',
            'description',
        )


class BookingCreateEditSerializer(serializers.ModelSerializer):

    def validate(self, obj):
        obj = super(BookingCreateEditSerializer, self).validate(obj)

        # check if drugs are available for specified pharmacy
        pharmacy = obj.get('pharmacy')
        drugs = obj.get('drugs')
        available_drugs_ids = pharmacy.available_drugs.all().values_list('id', flat=True)
        unavailable = [drug.name for drug in drugs if drug.id not in available_drugs_ids]
        if unavailable:
            raise ValidationError(f'{unavailable} - these are unavailable for this pharmacy')
        return obj

    class Meta:
        model = Booking
        fields = (
            'id',
            'patient',
            'pharmacy',
            'drugs',
            'created',
            'modified',
            'description',
        )
