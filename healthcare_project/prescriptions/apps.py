from django.apps import AppConfig


class PrescriptionsConfig(AppConfig):
    name = 'healthcare_project.prescriptions'
