from django.contrib import admin
from healthcare_project.prescriptions.models import Pharmacy, Booking, Drug, DrugCategory


@admin.register(Pharmacy)
class PharmacyModelAdmin(admin.ModelAdmin):

    def get_available_drugs_display(self, obj):
        return '\n'.join(obj.available_drugs.all().values_list('name', flat=True))

    get_available_drugs_display.short_description = 'Available Drugs'

    list_display = (
        'name',
        'address',
        'city',
        'state',
        'zipcode',
        'get_available_drugs_display',
    )


@admin.register(Booking)
class BookingModelAdmin(admin.ModelAdmin):

    def get_drugs_display(self, obj):
        return '\n'.join(obj.drugs.all().values_list('name', flat=True))

    get_drugs_display.short_description = 'Drugs'

    list_display = (
        'patient',
        'pharmacy',
        'get_drugs_display',
        'created',
        'modified',
        'description'
    )


@admin.register(Drug)
class DrugModelAdmin(admin.ModelAdmin):

    def get_categories_display(self, obj):
        return '\n'.join(obj.categories.all().values_list('name', flat=True))

    get_categories_display.short_description = 'Categories'

    list_display = (
        'name',
        'description',
        'get_categories_display'
    )


@admin.register(DrugCategory)
class DrugCategoryModelAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'description',
    )

