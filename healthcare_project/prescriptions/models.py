from django.db import models
from django.utils.translation import gettext as _
from django.conf import settings


class Pharmacy(models.Model):
    """
    Pharmacy model
    Contains information for given pharmacy

    Used for drugs' bookings
    """

    name = models.CharField(_('name'), max_length=128)
    address = models.CharField(_('address'), max_length=128)
    city = models.CharField(_('city'), max_length=128)
    zipcode = models.CharField(_('zipcode'), max_length=11)
    state = models.CharField(_('state'), max_length=128)

    available_drugs = models.ManyToManyField('prescriptions.Drug')


class Drug(models.Model):
    """
    Drug model
    contains information for given drug
    """
    name = models.CharField(_('name'), max_length=128)
    description = models.CharField(_('description'), max_length=128)

    categories = models.ManyToManyField(
        'prescriptions.DrugCategory',
        related_name='categorized_drugs'
    )


class DrugCategory(models.Model):
    name = models.CharField(_('name'), max_length=128)
    description = models.CharField(_('description'), max_length=128)


class Booking(models.Model):
    patient = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name='ordered_bookings',
        null=True
    )
    pharmacy = models.ForeignKey(
        'prescriptions.Pharmacy',
        on_delete=models.SET_NULL,
        null=True,
        related_name='bookings'
    )
    description = models.CharField(max_length=128)
    drugs = models.ManyToManyField(
        'prescriptions.Drug',
        related_name='drugs_bookings'
    )

    created = models.DateTimeField(_('created'), auto_now_add=True)
    modified = models.DateTimeField(_('modified'), auto_now=True)

